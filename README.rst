Depcheck mark 2
=============================

Copyright 2013, 2014 John Dulaney <jdulaney@fedoraproject.org>
Copyright 2014 Red Hat, Inc.

Dependency checker for Fedora based on libsolv

As of right now, to invoke, run::

  python run_depcheck.py

If you append -h, it will print a friendly help message


Setting Up Development Environment
----------------------------------

You'll need the following RPMs installed (assuming F19 or newer):

  - python-solv

  - python-librepo

  - python-virtualenv

  - python-rpmfluff >= 0.4.2


Once those are installed, create a virtualenv using system site-packages::

  virtualenv --distribute --system-site-packages env_depcheck

You'll also need a working checkout of libtaskotron installed in the virtualenv.
Using the activated env_depcheck::

  pip install -e /path/to/libtaskotron/checkout


Running Functional Test Suite
-----------------------------

`PyTest <https://pytest.org/>`_ needs to be installed in the virtualenv
regardless of whether or not you have it installed on the system (to pick up the
libtaskotron that we installed). To install it::

  pip install -U pytest

You'll also need dingus:

  pip install -U dingus

Once the setup is complete, run the functional test suite by::

  py.test --functional tests/

If you want to see the output as the tests run, use::

  py.test -s --functional tests/

Read ``tests/depcheck_scenarios/README.md`` for more details.
