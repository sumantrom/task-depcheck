from dingus import Dingus
import shutil
import os
import glob
import pytest
import __builtin__
import gzip
import librepo

from depcheck import metadata


class TestDepcheckMetadata(object):
    def setup_method(self, method):
        pass


class TestDepcheckRemoteRepo(object):
    def setup_method(self, method):
        self.ref_arch = 'x86_64'
        self.ref_repopath = 'https://localhost/path/to/testing_repo'

        # repos is a dict of dicts {'name': {'url': url, 'arch': arch, 'localpath', localpath}
        self.ref_localrepo = {'arch': self.ref_arch, 'path': self.ref_repopath}
        self.ref_repos = {'local': self.ref_localrepo}

        self.ref_workdir = '/tmp/work'
        self.ref_repodir = '%s/repodata/local' % self.ref_workdir
        self.ref_primary_gzpath = '%s/primary.xml.gz' % self.ref_repodir
        self.ref_filelist_gzpath = '%s/filelists.xml.gz' % self.ref_repodir
        self.ref_gzfiles = {'primary': self.ref_primary_gzpath,
                            'filelists': self.ref_filelist_gzpath}

        self.ref_gz_repodata = {'local': self.ref_gzfiles}

    def test_depcheck_download_remote(self, monkeypatch):
        stub_download_repodata = Dingus('download')

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=False)
        monkeypatch.setattr(test_metadata, '_download_repo_metadata', stub_download_repodata)
        monkeypatch.setattr(shutil, 'copy', Dingus())

        test_metadata._handle_repo('local')

        assert len(stub_download_repodata.calls()) == 1

    def test_metadata_remote_nouncompress(self, monkeypatch):
        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=False)

        stub_download_metadata = Dingus('download', return_value=self.ref_gzfiles)
        monkeypatch.setattr(test_metadata, '_download_repo_metadata', stub_download_metadata)

        test_metadata._handle_repo('local')

        assert test_metadata.repodata_dict == self.ref_gz_repodata

    def test_download_remote_metadata(self, monkeypatch):
        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=False)

        # stub stuff out so that we don't touch the filesystem or network
        monkeypatch.setattr(librepo, 'Handle', Dingus('librepo'))
        monkeypatch.setattr(os, 'makedirs', Dingus('makedirs'))

        # ew, double mocking. not seeing a better way to do this ATM
        stub_resultobject = Dingus('result', getinfo__returns=self.ref_gzfiles)
        monkeypatch.setattr(librepo, 'Result', Dingus(return_value=stub_resultobject))

        test_repodata = test_metadata._download_repo_metadata(self.ref_repopath,
                                                              self.ref_repodir)

        assert test_repodata == self.ref_gzfiles


class TestDepcheckMetadataLocal(object):
    def setup_method(self, method):
        self.ref_arch = 'x86_64'
        self.ref_repopath = '/path/to/testing_repo'

        self.ref_localrepo = {'arch': self.ref_arch, 'path': self.ref_repopath}
        self.ref_repos = {'local': self.ref_localrepo}

        self.ref_primary_gzpath = '%s/repodata/local/primary.xml.gz' % self.ref_repopath
        self.ref_filelist_gzpath = '%s/repodata/local/filelists.xml.gz' % self.ref_repopath
        self.ref_repodata_gzfiles = {'primary': self.ref_primary_gzpath,
                                     'filelists': self.ref_filelist_gzpath}

        self.ref_workdir = '/tmp/work'

        self.ref_primary_path = '%s/repodata/local/primary.xml' % self.ref_workdir
        self.ref_filelist_path = '%s/repodata/local/filelists.xml' % self.ref_workdir

        self.ref_repodata_nogzfiles = {'primary': self.ref_primary_path,
                                       'filelists': self.ref_filelist_path}


    def test_depcheck_nodownload_local(self, monkeypatch):
        stub_handle_localrepo = Dingus('handle_repo')

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=False)
        monkeypatch.setattr(test_metadata, '_handle_local_repofiles', stub_handle_localrepo)

        test_metadata._handle_repo('local')

        assert len(stub_handle_localrepo.calls()) == 1

    def test_metadata_local_nouncompress(self, monkeypatch):
        ref_repodata = {'local': self.ref_repodata_gzfiles}

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=False)

        stub_handle_localrepo = Dingus('find_repofiles', return_value=self.ref_repodata_gzfiles)
        monkeypatch.setattr(test_metadata, '_handle_local_repofiles', stub_handle_localrepo)

        test_metadata._handle_repo('local')

        assert test_metadata.repodata_dict == ref_repodata

    def test_metadata_local_uncompress(self, monkeypatch):
        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=True)

        monkeypatch.setattr(test_metadata, '_handle_local_repofiles', Dingus('handle_repofiles'))

        stub_uncompress = Dingus('uncompress', return_value=self.ref_repodata_nogzfiles)
        monkeypatch.setattr(test_metadata, '_uncompress_files', stub_uncompress)

        test_metadata._handle_repo('local')

        assert test_metadata.repodata_dict == {'local': self.ref_repodata_nogzfiles}

    def test_local_create_missing_dir(self, monkeypatch):
        ref_targetdir = os.path.join(self.ref_workdir, 'local')

        stub_makedirs = Dingus('makedirs')

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=True)
        monkeypatch.setattr(shutil, 'copy', Dingus('shutil'))
        monkeypatch.setattr(test_metadata, '_find_repo_mdfiles', Dingus(return_value=self.ref_repodata_gzfiles))
        monkeypatch.setattr(os, 'makedirs', stub_makedirs)

        test_metadata._handle_local_repofiles(self.ref_repopath, ref_targetdir)

        # make sure only one call to makedirs was made
        assert len(stub_makedirs.calls()) == 1

        # verify that the correct directory was created
        assert stub_makedirs.calls[0][1][0] == ref_targetdir

    def test_local_missing_repofiles(self, monkeypatch):
        stub_glob = Dingus('glob', return_value=[])

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=True)
        monkeypatch.setattr(glob, 'glob', stub_glob)

        with pytest.raises(Exception):
            test_metadata._find_repo_mdfiles(self.ref_repopath)

    def test_local_findmdfiles(self, monkeypatch):
        ref_repopath = '/path/to/testing_repo'

        # this uses checksummed repomd files, which is different from what setup_method has
        ref_filelist_mdfilename = '%s/3a753e1d0a27fc2cf1cf4cb11d8f3a0c1a0ddede4886a996b46b2346a9998963-filelists.xml.gz' % ref_repopath
        ref_primary_mdfilename = '%s/eab787011e2ce6d7b14555118c1b44c1dd0d36af72f65f4feb5520615af2dc8a-primary.xml.gz' % ref_repopath
        ref_repodata_gzfiles = [ref_filelist_mdfilename, ref_primary_mdfilename]

        stub_glob = Dingus('glob', return_value=ref_repodata_gzfiles)
        monkeypatch.setattr(glob, 'glob', stub_glob)

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=True)
        test_repomd_filenames = test_metadata._find_repo_mdfiles(ref_repopath)

        assert test_repomd_filenames == {'primary': ref_primary_mdfilename,
                                         'filelists': ref_filelist_mdfilename}

    def test_metadata_output_first_call(self, monkeypatch):
        stub_verify = Dingus('verify')

        test_metadata = metadata.DepcheckMetadata(self.ref_repos)
        monkeypatch.setattr(test_metadata, 'verify', stub_verify)

        test_repodata = test_metadata.repodata

        assert len(stub_verify.calls()) == 1

    def test_metadata_output_single_local(self):
        test_metadata = metadata.DepcheckMetadata(self.ref_repos)
        test_metadata.repodata_dict = self.ref_repodata_gzfiles

        test_output = test_metadata.repodata

        assert test_output == self.ref_repodata_gzfiles

    def test_metadata_uncompress_paths(self, monkeypatch):
        ref_targetdir = os.path.join(self.ref_workdir, 'repodata/local')

        test_metadata = metadata.DepcheckMetadata(self.ref_repos,
                                                  uncompress=True)

        # don't actually touch the filesystem
        monkeypatch.setattr(__builtin__, 'open', Dingus('open'))
        monkeypatch.setattr(gzip, 'open', Dingus('gzip_open'))

        test_repodata = test_metadata._uncompress_files(self.ref_repodata_gzfiles, ref_targetdir)

        assert test_repodata == self.ref_repodata_nogzfiles
