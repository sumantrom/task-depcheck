import pytest
from dingus import Dingus
import __builtin__

from depcheck import squash_results
from depcheck.squash_results import _squash_builds_to_updates, _squash_rpms_to_builds
from depcheck import Pass, Fail


class TestSquashResults(object):
    def test_rpms_to_builds_pass(self):
        """
        Checks whether _squash_rpms_to_builds provides correct output
        when all rpms are 'Pass'.
        """
        run_results = {
                'rpm1': {'result': Pass, 'details': []},
                'rpm2': {'result': Pass, 'details': ["moo"]},
                }
        fake_koji = Dingus(rpms_to_build__returns = [{'nvr': 'nvr1'},
                                                     {'nvr': 'nvr1'}])
        outcome = _squash_rpms_to_builds(run_results, fake_koji)

        assert len(outcome.keys()) == 1
        assert outcome['nvr1']['result'] == Pass
        assert outcome['nvr1']['details'] == []

    def test_rpms_to_builds_fail(self):
        """
        Checks _squash_rpms_to_builds outcome for a `Fail` rpm.
        """
        run_results = {
                'rpm1': {'result': Pass, 'details': []},
                'rpm2': {'result': Fail, 'details': ["foo bar"]},
                'rpm3': {'result': Pass, 'details': ["moo"]},
                }
        fake_koji = Dingus(rpms_to_build__returns = [{'nvr': 'nvr1'},
                                                     {'nvr': 'nvr1'},
                                                     {'nvr': 'nvr1'}])
        outcome = _squash_rpms_to_builds(run_results, fake_koji)

        assert len(outcome.keys()) == 1
        assert outcome['nvr1']['result'] == Fail
        assert outcome['nvr1']['details'] == ["foo bar"]


    def test__squash_builds_to_updates_all_ok(self):
        """
        Checks whether __squash_builds_to_updates provides correct output
        when all the builds in an update passed depchecking.
        """

        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Pass, 'details': []},
                }
        update = {'alias': 'update_name',
                  'builds': [{'nvr': k} for k in run_results.keys()],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        build_2_update = (
                {
                    'nvr1': update,
                    'nvr2': update,
                }, {})

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        assert len(cds) == 1

        detail = cds[0]
        assert detail.item == update['alias']
        assert detail.outcome == 'PASSED'

    def test__squash_builds_to_updates_build_failed(self):
        """
        Checks whether __squash_builds_to_updates provides correct output
        when some of the builds in an update failed depchecking.
        """
        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Fail, 'details': ['### TEST @@@']},
                }
        update = {'alias': 'update_name',
                  'builds': [{'nvr': k} for k in run_results.keys()],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        build_2_update = (
                {
                    'nvr1': update,
                    'nvr2': update,
                }, {})

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        assert len(cds) == 1

        detail = cds[0]
        assert detail.item == update['alias']
        assert detail.outcome == 'FAILED'
        assert '### TEST @@@' in detail.output

    def test__squash_builds_to_updates_incomplete_update(self):
        """
        Checks whether __squash_builds_to_updates provides correct output
        when some update is incomplete (i.e. present in the second return-value
        from the build2update method).
        """
        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Pass, 'details': []},
                }
        update = {'alias': 'update_name',
                  'builds': [{'nvr': k} for k in run_results.keys()],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        update['builds'].append({'nvr':'nvr3'})
        build_2_update = ({},
                {
                    'nvr1': update,
                    'nvr2': update,
                })

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        assert len(cds) == 1

        detail = cds[0]
        assert detail.item == update['alias']
        assert detail.outcome == 'ABORTED'
        assert any('Update was incomplete' in line for line in detail.output)

    def test__squash_builds_to_updates_incomplete_update_missing_first(self):
        """
        Checks whether __squash_builds_to_updates provides correct output
        when some update is incomplete (i.e. present in the second return-value
        from the build2update method) and the missing build is the first one
        to process (https://phab.qadevel.cloud.fedoraproject.org/T476).
        """
        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Pass, 'details': []},
                }
        update = {'alias': 'update_name',
                  'builds': [{'nvr': k} for k in run_results.keys()],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        # the builds are processed alphabetically in _squash_build_to_updates(),
        # so naming it 'abc1' ensures it is processed before 'nvr*'
        update['builds'].append({'nvr':'abc1'})
        build_2_update = ({},
                {
                    'nvr1': update,
                    'nvr2': update,
                })

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        assert len(cds) == 1

        detail = cds[0]
        assert detail.item == update['alias']
        assert detail.outcome == 'ABORTED'
        assert any('Update was incomplete' in line for line in detail.output)

    def test__squash_builds_to_updates_missing_update(self):
        """
        Checks whether __squash_builds_to_updates provides correct output
        when there is no update for a certain build.
        """
        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Pass, 'details': []},
                }
        update = {'alias': 'update_name',
                  'builds': [{'nvr': 'nvr1'}],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        build_2_update = (
                { 'nvr1': update, },
                { 'nvr2': None, },
                )

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        # 'nvr2' got ignored (just logged), so only one CheckDetail got returned
        assert len(cds) == 1
        detail = cds[0]
        assert detail.item == update['alias']
        assert detail.outcome == 'PASSED'

    def test__squash_builds_to_updates_missing_alias(self):
        """
        Checks to make sure that missing alias is properly handled
        """
        run_results = {
                'nvr1': {'result': Pass, 'details': []},
                'nvr2': {'result': Pass, 'details': []},
                }
        update = {'title': 'update_name',
                  'alias': None,
                  'builds': [{'nvr': 'nvr1'}],
                  'release': {'name': 'F20'},
                  'request': 'stable',
                 }
        build_2_update = (
                { 'nvr1': update, },
                { 'nvr2': None, },
                )

        fake_bodhi = Dingus(build2update__returns = build_2_update)

        cds = _squash_builds_to_updates(run_results, fake_bodhi)
        detail = cds[0]
        assert detail.item == update['title']
        assert detail.outcome == 'PASSED'

