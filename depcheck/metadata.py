# metadata.py - handling metadata retrieval and uncompression for depcheck
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Copyright 2013, 2014 John H. Dulaney
# Copyright 2014 Red Hat, Inc
#
# Authors:
#   Tim Flink <tflink@redhat.com>
#   John H. Dulaney <jdulaney@fedoraproject.org>

import librepo
import tempfile
import os
import glob
import gzip
import shutil
import sys

from . import log

class DepcheckMetadata(object):
    def __init__(self, repos, targetdir=None, uncompress=True):
        """ Handle yum repo metadata and prepare it for use by Depcheck.

        Repository information is passed in as a dict of dicts:
          {'reponame': {'path': path_to_repo, 'arch': arch}}

        path can be either a local directory containing repomd files or a URL
        which points to a remote (http) repository

        By default, the repodata will be copied to the local targetdir and
        uncompressed (gzip only).

        :param repos: dict containing a dict of repo path and arch
        :type repos: dict
        :param targetdir: base directory where uncompressed repomd files should be stored
        :type targetdir: str
        :param uncompress: Indication of whether or not to uncompress repofiles
        :type uncompress: bool
        """
        self.uncompress = uncompress

        # repos is a dict of dicts {'name': {'remote': url, 'arch': arch, 'localpath', localpath}
        self.repos = repos

        # repodata is eventually a dict of {reponame: {filelists: '/path/to/filelists', 'primary': '/path/to/primary'}}
        self.repodata_dict = {}

        if targetdir is None:
            self.targetdir = tempfile.mkdtemp('', 'depcheck-', '/var/tmp/')
        else:
            self.targetdir = targetdir


    def hmfcb(_, msg, url, metadata, __):
        log.logger.warning("Mirror Error:  %s %s" % (msg, url))


    def _download_repo_metadata(self, url, targetdir,
                                mdtypes=['filelists', 'primary']):
        """ Download repomd files from the specified remote repository. Repository
        metalinks and direct links are both supported with no need for input from
        the user.

        :param url: URL to the remote repository
        :type url: str
        :param targetdir: local path to directory into which repomd files should be downloaded
        :type targetdir: str
        :param mdtypes: repomd file types to download
        :type mdtypes: list of str
        :returns: dict of {mdtype: filepath, ...}
        """

        os.makedirs(targetdir)

        handle = librepo.Handle()
        handle.setopt(librepo.LRO_REPOTYPE, librepo.LR_YUMREPO)
        if 'metalink' in url:
            handle.setopt(librepo.LRO_MIRRORLIST, url)
        else:
            handle.setopt(librepo.LRO_URLS, url)
        handle.setopt(librepo.LRO_REPOTYPE, librepo.LR_YUMREPO)

        # Destination directory for metadata
        handle.setopt(librepo.LRO_DESTDIR, targetdir)
        handle.setopt(librepo.LRO_YUMDLIST, mdtypes)
        handle.setopt(librepo.LRO_HMFCB, self.hmfcb)
        result = librepo.Result()

        try:
            handle.perform(result)
        except librepo.LibrepoException as error:
            # rc - Return code (integer value)
            # msg - Detailed error message (string)
            # general_msg - Error message based on rc (string)
            rc, msg, general_msg = error
            # this needs to be handled better, is temporary
            sys.tracebacklimit=0
            raise Exception("Librepo Error %d: %s \n" % (rc, msg))

        repo_downloads = result.getinfo(librepo.LRR_YUM_REPO)

        repodata = {}
        for mdtype in mdtypes:
            repodata[mdtype] = repo_downloads[mdtype]

        return repodata

    def _find_repo_mdfiles(self, repodir, mdtypes=['filelists', 'primary']):
        """Find repomd files by type in a local directory.

        :param repodir: Local path to repository from which metadata files are desired
        :type repodir: str
        :param mdtypes: list of mdfile types to find
        :type mdtypes: list of str
        :returns: {'primary': primaryxml_gzfile_path, 'filelists': filelists_gzfile_path}
        """

        # assuming that repo is a local path for now
        metadata_gzfiles = glob.glob('%s/repodata/*.gz' % repodir)
        repo_mdfiles = {}

        for gzfile in metadata_gzfiles:
            if gzfile.endswith('primary.xml.gz'):
                repo_mdfiles['primary'] = gzfile
            elif gzfile.endswith('filelists.xml.gz'):
                repo_mdfiles['filelists'] = gzfile

        for mdtype in mdtypes:
            if not mdtype in repo_mdfiles:
                # this needs to be handled better, is temporary
                raise Exception("Not able to find requested metadata file for %s in %s" % (mdtype, repodir))
        return repo_mdfiles

    def _uncompress_files(self, repofiles, target_dir):
        """ Pure python code to extract contents of gzipped files into a
        directory, removing the '.gz' file extension. Does not delete the
        original gzipped files after extraction

         :param repofiles: List of absolute paths to files to be uncompressed
         :type repofiles: list of str
         :param target_dir: path to extract files into
         :type target_dir: str
         :returns: list of extracted files
        """
        new_repofiles = {}

        for mdtype in repofiles:
            new_filename = os.path.basename(repofiles[mdtype]).replace('.gz', '')
            new_filepath = os.path.join(target_dir, new_filename)

            try:
                f_in = gzip.open(repofiles[mdtype], 'rb')
                f_out = open(new_filepath, 'wb')
                f_out.writelines(f_in)
                f_out.close()
                f_in.close()
            except IOError as e:
                # this needs to be handled better, is temporary
                log.logger.debug(e)
                raise Exception("Error uncompressing mdfile %s to %s" % (repofiles[mdtype], new_filepath))

            new_repofiles[mdtype] = new_filepath
        return new_repofiles

    def _handle_local_repofiles(self, repopath, target_repodir):
        """ Find repomd files in a local repository and copy them to the target
         directory.

         :param repopath: Absolute path to local repository
         :type repopath: str
         :param target_repodir: Absolute path to target directory
         :type target_repodir: str
         :returns: dict of {mdtype: path, ...}
        """
        # find files, make sure we have what we need
        local_repofiles = self._find_repo_mdfiles(repopath)

        repofiles = {}

        # copy files to the target dir if they do not already exist there
        if not os.path.isdir(target_repodir):
            os.makedirs(target_repodir)

        for repomdfile in local_repofiles:
            repofilename = os.path.basename(local_repofiles[repomdfile])
            target_filename = os.path.join(target_repodir, repofilename)
            if not os.path.exists(target_filename):
                shutil.copy(local_repofiles[repomdfile], target_filename)
            repofiles[repomdfile] = target_filename

        return repofiles

    def _handle_repo(self, reponame):
        """ Determine whether a repository is local or remote, obtain the needed
         files and optionally uncompress repomd files from the specified
         repository.

         :param reponame; Name of the repository to handle
         :type reponame: str
        """
        single_repodata = self.repos[reponame]
        repo_location = self.repos[reponame]['path']

        repo_targetdir = os.path.join(self.targetdir, 'repodata', reponame)

        islocal = not repo_location.startswith('http')

        # at this point, we want copies of the mdfiles in our target dir
        # regardless of whether the repo is remote or local
        if islocal:
            repofiles = self._handle_local_repofiles(single_repodata['path'],
                                                     repo_targetdir)
        else:
            repofiles = self._download_repo_metadata(repo_location, repo_targetdir)

        if self.uncompress:
            unzip_repofiles = self._uncompress_files(repofiles, repo_targetdir)
            self.repodata_dict[reponame] = unzip_repofiles
        else:
            self.repodata_dict[reponame] = repofiles

    def verify(self):
        """ Locate and prepare repomd files for all repositories submitted at
         creation time.
        """
        for reponame in self.repos:
            self._handle_repo(reponame)

    @property
    def repodata(self):
        """ Ensure that repmd files have been properly prepared and return a
        dict of those prepared files, by repository name

        :returns: {reponame: {mdfiletype: path, ...}, ...}
        """
        if len(self.repodata_dict) < len(self.repos):
            self.verify()

        return self.repodata_dict
