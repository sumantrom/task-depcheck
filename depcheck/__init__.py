#!/usr/bin/python
#
# Copyright 2013 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Depcheck mk 2
# EPEL dep:  python-argparse
"""Depcheck mk 2 is a new dependency checker for rpms and yum repositories"""

__version__ = '0.0.5'


# Check to see if libtaskotron is present; if so, import pass and fail

# It is crucial that these are the same as libtaskotron/check possible outcomes
Pass = 'PASSED'
Fail = 'FAILED'
Aborted = 'ABORTED'

import os.path
import sys
import argparse
import tempfile
import platform
import logging

from . import depcheck
from . import metadata
from . import exc
from . import squash_results
from . import log

from libtaskotron.python_utils import iterable


def dir_to_rpms(dirname):
    """Find all rpms in the specified directory
    :param dirname: local directory in which to look for rpms
    :type dirnema: str
    :returns: list of rpm files with absolute, local path
    """
    rpm_list = []
    for filename in os.listdir(dirname):
        if filename.endswith('.rpm'):
            rpm_list.append(os.path.join(dirname, filename))
    return rpm_list


def extract_named_repo(namedrepo):
    return namedrepo.split(':', 1)

def run(rpms, arch, repos, report_format="updates", workdir=None, artifactsdir=None):
    """
    report_format: rpms, updates
    """
    # handle directory args, expanding them to a list of rpm files with absolute
    # paths
    if isinstance(rpms, basestring):
        rpms = [rpms]
    if not iterable(rpms, basestring):
        raise exc.DepcheckException("rpms argument must be either string"
                "or list of strings")

    rpmlist = []
    for filename in rpms:
        if os.path.isdir(filename):
            rpmlist.extend(dir_to_rpms(filename))
        else:
            rpmlist.append(filename)

    # arm has some special case requirements for arch handling
    # Fixup for arm; the yum repo is labled as armhfp but the nvr is armv7hl
    if arch == 'armv7l':
        arch = 'armhfp'

    # fix arch for libsolv
    if arch == 'i386':
        arch = 'i686'

    # handle data formatting for repo metadata (list of dicts)
    log.logger.debug("Preparing metadata download")
    handled_repos = {}
    for repo, path in repos.items():
        log.logger.debug("  - %r: %r, %r", repo, arch, path)
        handled_repos[repo] = {'arch': arch, 'path': path}

    if workdir is None:
        workdir = tempfile.mkdtemp('', 'depcheck-', '/var/tmp/')
    else:
        # FIXME - fix properly, sometimes. The reason for adding arch
        #  is that in runtask.yml there are two runs of the depcheck
        #  right after each other.
        workdir = os.path.join(workdir, 'depcheck_workdir_%s' % arch)
        os.makedirs(workdir)

    log.logger.info("Getting metadata...")
    # FIXME this call takes a long time, log progress gradualy so buildbot won't
    #   kill the process
    md_handle = metadata.DepcheckMetadata(handled_repos, targetdir=workdir)

    repomd_files = md_handle.repodata

    log.logger.info("Running Depcheck")
    depck = depcheck.Depcheck(str(arch), repomd_files, workdir=workdir)
    run_result = depck.check_rpm_deps(rpmlist)

    log.logger.info("Formatting output")

    # Sometimes, depcheck gets run with no rpms
    if not run_result:
        log.logger.warn("No results")

    output = squash_results.format_output(run_result, arch, report_format, artifactsdir)
    log.logger.info(output)

    return output

def depcheck_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("rpm", nargs='+', help="RPMs to be tested")
    parser.add_argument("-d", "--debug", action="store_true", help="enable debug logging for depcheck")
    parser.add_argument("-a", "--arch", choices=["i386", "x86_64", "armhfp"], help="""
                        Architecture to be tested. If omitted, defaults to the current
                        machine's architecture.
                        """)
    parser.add_argument("-r", "--repo", action="append", dest="repos", help="""
                        Repo to be tested against. Repo can be local (in the form /dir/of/repo)
                        or remote (direct link or standard metalink).
                        Can be used multiple times, to specify more repos.
                      """)
    parser.add_argument("-f", "--format", choices=["rpms", "updates"], help="""
                        Specify output format to override configuration.
                        """)
    parser.add_argument("-w", "--workdir", help="workdir to use for depcheck")

    args = parser.parse_args()

    # initialize logging when entering depcheck via cli - this won't be executed
    # otherwise but in that case, taskotron is responsible for initialization
    logging.basicConfig()

    if args.debug:
        log.logger.setLevel(logging.DEBUG)

    # autodetect arch if none is passed in
    arch = args.arch
    if arch is None:
        arch = platform.machine()


    repos = dict([(repo, repo) for repo in args.repos])

    try:
        run(args.rpm, arch, repos, args.format, args.workdir)
    except KeyboardInterrupt:
        print >> sys.stderr, ("\n\nExiting on user cancel.")
        sys.exit(1)


