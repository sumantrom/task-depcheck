import os
import json

from . import Pass, Fail
from . import log

from libtaskotron.ext.fedora.koji_utils import KojiClient
from libtaskotron.ext.fedora.bodhi_utils import BodhiUtils
from libtaskotron import check

def _squash_rpms_to_builds(run_result, koji_client = None):
    """
    Converts the per-rpm results created by check_rpm_deps into per-build.
    :param run_result: output of the check_rpm_deps method
    :type run_result: dict
    :returns: dict containing per-nvr results
    """
    if koji_client is None:
        koji_client = KojiClient()

    per_nvr = {}
    rpms = run_result.keys()

    # rpms_to_build guarantees that order of buildinfos in list will be the
    # same as the order of their respective rpms
    buildinfos = koji_client.rpms_to_build(rpms)
    for rpm, buildinfo in zip(rpms, buildinfos):
        nvr = buildinfo['nvr']

        if nvr in per_nvr:
            if per_nvr[nvr]['result'] == Pass and \
                            run_result[rpm]['result'] == Fail:
                per_nvr[nvr]['result'] = Fail
        else:
            per_nvr[nvr] = {
                    'result': run_result[rpm]['result'],
                    'details': run_result[rpm]['details'],
                    }

        if run_result[rpm]['result'] == Fail:
            per_nvr[nvr]['details'].extend(run_result[rpm]['details'])

    return per_nvr


def _squash_builds_to_updates(results_nvr, bodhi_client = None):
    """
    Converts the per-build results created by _squash_rpms_to_builds into
    per-update results.
    :param results_nvr: output of the _squash_rpms_to_builds
    :type results_nvr: dict
    :returns: list of CheckDetail
    """
    if bodhi_client is None:
        bodhi_client = BodhiUtils()

    pending_ok, pending_fail = bodhi_client.build2update(results_nvr.keys(), strict = True)

    updates = {}
    for _, update in pending_ok.items():
        update_name = update.get('alias') or update['title']
        updates[update_name] = update


    for build, update in pending_fail.items():
        if update is None:
            log.logger.error("Build without an update: %r" % build)
            continue
        update_name = update.get('alias') or update['title']
        if update_name not in updates:
            updates[update_name]= update


    updates_results = []
    for update_alias, update in updates.items():
        cd = check.CheckDetail(update_alias, check.ReportType.BODHI_UPDATE, outcome = 'PASSED',
                               checkname='depcheck')
        updates_results.append(cd)

        builds = sorted([build['nvr'] for build in update['builds']])

        if set(builds).intersection(set(pending_fail.keys())):
            # A build is in pending_fail either when it has no assigned
            #  update (that can not happen, as we traverse builds of
            #  a specific update), or when an update was incomplete
            #  (e.g. missing some builds).
            # If an update is incomplete, all its builds are put
            #  into the pending_fail.
            cd.update_outcome('ABORTED')
            cd.store('Update was incomplete as downloaded at execution '
                     'time and not all builds have been tested. This can '
                     'happen if the update is edited in Bodhi during check '
                     'execution.', printout=False)
            expected = set(builds)
            tested = set([bld for bld, upd in pending_fail.items() if upd is update])
            missing = expected.difference(tested)
            cd.store("Builds which haven't been tested:", printout=False)
            for bld in sorted(missing):
                cd.store('  %s' % bld, printout=False)
        else:
            for build in builds:
                if results_nvr[build]['result'] != Pass:
                    cd.update_outcome(results_nvr[build]['result'])
                    cd.store('Build %s failed depcheck' % build, printout=False)
                    cd.output.extend(results_nvr[build]['details'])

        # set note
        cd.note = '%s %s' % (update['release']['name'],
                             update['request'] if 'request' in update else update['status'])

    return updates_results


def squash_results(run_result, bodhi_client = None, koji_client = None):
    per_nvr = _squash_rpms_to_builds(run_result, koji_client)
    per_update = _squash_builds_to_updates(per_nvr, bodhi_client)

    return per_update

def format_output(run_result, arch, report_format="rpms", artifactsdir=None):
    if not run_result:
        return check.export_YAML([])

    if 'updates' or 'builds' in report_format:
        results = []

        build_results = _squash_rpms_to_builds(run_result)
        if 'builds' in report_format:
            for name, status in build_results.iteritems():
                cd = check.CheckDetail(name, check.ReportType.KOJI_BUILD, status['result'])
                cd.output.extend(status['details'])
                results.append(cd)

        if 'updates' in report_format:
            update_results = _squash_builds_to_updates(build_results)
            results.extend(update_results)

        # remove the arch conversion to be consistent with cmdline arch
        if arch == 'i686':
            arch = 'i386'
        for result in results:
            result.keyvals['arch'] = arch

        store_artifacts(results, artifactsdir)
        output = check.export_YAML(results)

    else:
        output = json.dumps(run_result, indent=4, separators=(',', ': '))

    return output

def store_artifacts(results, artifactsdir=None):
    '''Take a list of CheckDetails and save it as inidividual log files in
    the artifactsdir directory, provided it is defined.
    The method also sets the artifact attribute for each result item as
    a side effect.
    '''

    if not artifactsdir:
        log.logger.debug('Not writing individual logs, not requested')
        return

    try:
        storedir = os.path.abspath(artifactsdir)
        log.logger.debug('Writing individual logs into: %s', storedir)

        if not os.path.exists(storedir):
            os.makedirs(storedir)

        # save logs
        for result in results:
            item = result.item
            filename = "%s.%s.log" % (item, result.keyvals['arch'])

            # Bodhi update titles can be very long, we need to trim them
            if len(filename) > 255:  # the max file name length for most Linux filesystems
                exceed_len = len(filename) - 255
                # length of ellipsis, .log and arch to make total length 255 char
                extra_bits = 3 + 4 + len(result.keyvals['arch'])
                item = item[:-exceed_len-extra_bits] + '...'
                # don't need . separating item and arch since item ends with ... now
                filename = "%s%s.log" % (item, result.keyvals['arch'])
                assert len(filename) <= 255

            filepath = os.path.join(storedir, filename)
            fout = open(filepath, 'w')
            fout.write(check.export_YAML(result))
            fout.write('\n\n')
            fout.write('\n'.join(result.output))
            fout.close()
            result.artifact = filepath

    except OSError as e:
        log.logger.error('Individual logs not saved into %s, an error occured. '
                        'Raising.', artifactsdir)
        raise e

