# depcheck.py - core logic and interfaces for checking dependency trees
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Copyright 2013, 2014 John H. Dulaney
# Copyright 2014 Red Hat, Inc
#
# Authors:
#   John H. Dulaney <jdulaney@fedoraproject.org>
#   Tim Flink <tflink@redhat.com>
#
# EPEL dep:  python-argparse


import solv
import tempfile

from . import Pass, Fail, Aborted
from . import log


class Depcheck(object):
    def __init__(self, arch, repodata, workdir=None):
        """Depcheck uses existing repodata to check a set of rpm files for
        depsolving issues.

        This class itself doesn't handle much other than checking rpms and all
        data prep needs to be handled externally

        :param arch: The repository arch to use for dependency solving
        :type arch: str (not unicode)
        :param repodata: local paths to downloaded and verified repodata
        :type repodata: dir of dir {'reponame': {'primary': primarygz_filename, 'filelists': filelists_filename}...}
        :param workdir: The directory to use for depcheck work, a temp directory will be created if workdir is None
        :type workdir: str
        """

        self.arch = arch
        self.repos = repodata
        self.solvpool_object = None
        self.solvrepo_object = None
        if workdir is None:
            self.workdir = tempfile.mkdtemp('', 'depcheck-', '/var/tmp/')
        else:
            self.workdir = workdir

    @property
    def solvpool(self):
        if self.solvpool_object is None:
            self.solvpool_object = solv.Pool()
            self.solvpool_object.setarch(self.arch)
        return self.solvpool_object

    @property
    def solvrepo(self):
        if self.solvrepo_object is None:
            self.solvrepo_object = self.solvpool.add_repo(self.arch)
        return self.solvrepo_object

    def _prepare_libsolv(self):
        """Initialize libsolv and add the needed repository information for
        checking dependencies for the supplied repositories
        """

        log.logger.debug('preparing libsolv repos for depcheck run')

        for repo in self.repos:
            primary = open(self.repos[repo]['primary'], 'rb')
            filelist = open(self.repos[repo]['filelists'], 'rb')

            self.solvrepo.add_rpmmd(primary, None)
            self.solvrepo.add_rpmmd(filelist, None,
                                    solv.Repo.REPO_EXTEND_SOLVABLES)

    def _check_rpm(self, rpmfile, solvable):
        """Check a single rpm file against prepared repodata
         :param rpmfile: filename of rpm file to check
         :type rpmfile: str
         :param solvable: libsolv solvable from solvpool for the rpm to check
         :type solvable: libsolv.Solver.Solvable
        """

        # the job setup here is mostly taken from libsolv's installcheck tool
        # create a job using the predetermined solvable in the repo
        job = self.solvpool.Job(solv.Job.SOLVER_INSTALL | solv.Job.SOLVER_SOLVABLE,
                solvable.id)
        self.solvpool.set_flag(solv.Solver.SOLVER_FLAG_IGNORE_RECOMMENDED, 1)

        # solve the deps for the input solvable's rpm and look for problems
        solver = self.solvpool.Solver()
        solv_problems = solver.solve([job])

        if len(solv_problems) == 0:
             return {'result': Pass,'details':[]}
        else:
            problem_strs = []
            result = Fail
            for problem in solv_problems:
                probrules = problem.findallproblemrules(unfiltered=1)
                for rule in probrules:
                    ruleinfos = rule.allinfos()
                    for info in ruleinfos:
                        problem_str = info.problemstr()
                        problem_strs.append(problem_str)
                        if 'inferior architecture' in problem_str:
                            result = Aborted
                            warning_string = """**** PLEASE NOTE ****: This \
failure is likely invalid, caused by a bug we haven't been able to reproduce reliably or fix yet: \
https://phab.qadevel.cloud.fedoraproject.org/T366 . Please test your package \
dependencies manually and if everything looks correct, ignore this error. We \
apologize for the inconvenience and are working to get it fixed as soon as possible."""
                            problem_strs.append(warning_string)
            return {'result':result, 'details':problem_strs}


    def check_rpm_deps(self, rpmfiles):
        """Check dependencies of the given rpms against supplied repodata

        :param rpmfiles: local absolute paths to rpms to check
        :type rpmfiles: list of str
        :returns: dictionary of results {rpm: {'results': Pass/Fail, 'details': []}}
        """

        log.logger.debug('adding %d rpms to libsolv repos and preparing solvpool',
                         len(rpmfiles))


        # add all rpmfiles that we're checking to the libsolv repo and save the
        # solvable that is assigned to them.
        # this makes libsolv aware of all the incoming rpms and allows it to
        # check for any dep breakage between the rpms
        self._prepare_libsolv()
        solvables = {}
        for rpmfile in rpmfiles:
            rpmname = rpmfile.split('/')[-1]
            solvable = self.solvrepo.add_rpm(rpmfile)
            solvables[rpmname] = solvable

        # now that the rpms have been added, prepare the solvpool
        self.solvpool.addfileprovides()
        self.solvpool.createwhatprovides()


        # check each rpm using its solvable that was assigned while adding to
        # the repo
        results = {}
        for rpmname in sorted(solvables.keys()):
            log.logger.debug('running depcheck for %s' % rpmname)
            run_result = self._check_rpm(rpmname, solvables[rpmname])
            results[rpmname] = run_result

        return results


